module blacktazz/driveExporter

go 1.12

require (
	golang.org/x/net v0.0.0-20190313220215-9f648a60d977
	golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421
	google.golang.org/api v0.2.0
)
