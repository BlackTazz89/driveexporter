package main

import (
	"blacktazz/driveExporter/google"
	"compress/gzip"
	"crypto/md5"
	"encoding/gob"
	"flag"
	"fmt"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/googleapi"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"time"
)

var googleToCanonicalMime = map[string]string{
	"application/vnd.google-apps.drawing":      "image/png",
	"application/vnd.google-apps.document":     "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
	"application/vnd.google-apps.spreadsheet":  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
	"application/vnd.google-apps.presentation": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
	"application/vnd.google-apps.script+json":  "application/vnd.google-apps.script+json",
	"application/vnd.google-apps.script":       "application/vnd.google-apps.script+json",
}

var mimeToExt = map[string]string{
	"application/vnd.google-apps.drawing":      ".png",
	"application/vnd.google-apps.document":     ".docx",
	"application/vnd.google-apps.spreadsheet":  ".xlsx",
	"application/vnd.google-apps.presentation": ".pptx",
	"application/vnd.google-apps.script+json":  ".json",
	"application/vnd.google-apps.script":       ".json",
}

type downloader interface {
	Download(opts ...googleapi.CallOption) (*http.Response, error)
}

var filepathToMDate = make(map[string]string)

var fileTorename = make(map[string]string)

var duplicatesMutex sync.Mutex

const DefaultDstdFolder = "download"
const DefaultStateFile = "state.save"

type state struct {
	m      sync.Mutex
	hashes map[string]bool
}

type myDrive struct {
	inner     *drive.Service
	throttler <-chan time.Time
	s         state
	wg        sync.WaitGroup
	m         sync.Mutex
}

type fileQuery struct {
	text     string
	fields   googleapi.Field
	pageSize int64
	filter   func(file *drive.File) bool
}

func initState() (s state) {
	fi, err := os.OpenFile(DefaultStateFile, os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Fatalf("Impossible to open state file for reading. Error: %v\n", err)
	}

	s.hashes = make(map[string]bool)
	reader, err := gzip.NewReader(fi)
	if err != nil {
		if err != io.EOF {
			log.Fatalf("Impossible to create reader. Error: %v\n", err)
		}
		log.Println("Impossible to read state, maybe first run?")
		return
	}
	defer reader.Close()

	dec := gob.NewDecoder(reader)
	err = dec.Decode(&s.hashes)
	log.Printf("File saved in state: %d\n", len(s.hashes))
	if err != nil {
		log.Printf("Error while decoding state data: Error: %v\n", err)
	}
	return
}

func createDir(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return os.Mkdir(path, 0740)
	}
	return nil
}

func insertTimestamp(filename string, date string) string {
	dotIndex := strings.LastIndex(filename, ".")
	if dotIndex == -1 {
		dotIndex = len(filename)
	}
	return fmt.Sprintf("%s(at %s)%s", filename[0:dotIndex], strings.ReplaceAll(date, ":", "-"), filename[dotIndex:])
}

func createUniquePath(folderPath, name string, date string) string {
	// removing invalid character in directory name
	re := regexp.MustCompile(`[/\\\*\?:"<>|]`)
	newName := re.ReplaceAllString(name, "_")
	destPath := filepath.Join(folderPath, newName)

	duplicatesMutex.Lock()
	if existingDate, exists := filepathToMDate[destPath]; exists {
		if _, ok := fileTorename[destPath]; !ok {
			fileTorename[destPath] = filepath.Join(folderPath, insertTimestamp(newName, existingDate))
		}
		newName = insertTimestamp(newName, date)
	} else {
		filepathToMDate[destPath] = date
	}
	duplicatesMutex.Unlock()
	return strings.TrimSpace(newName)
}

func md5sum(s string) string {
	hash := md5.Sum([]byte(s))
	return string(hash[:])
}

func (s *state) isNewFile(folderPath string, f *drive.File) bool {
	re := regexp.MustCompile(`[/\\\*\?:"<>|]`)
	newName := strings.TrimSpace(re.ReplaceAllString(f.Name+mimeToExt[f.MimeType], "_"))
	destPath := filepath.Join(folderPath, newName)

	_, exists := s.hashes[md5sum(destPath+f.ModifiedTime)]
	if exists {
		return false
	}
	// Maybe was saved with the timestamp
	_, exists = s.hashes[md5sum(filepath.Join(folderPath, insertTimestamp(newName, f.ModifiedTime))+f.ModifiedTime)]
	return !exists

}

func (s *state) save(filepath string, f *drive.File) error {
	hash := md5sum(filepath + f.ModifiedTime)
	s.m.Lock()
	fo, err := os.OpenFile(DefaultStateFile, os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}
	writer := gzip.NewWriter(fo)
	defer writer.Close()

	s.hashes[hash] = true

	enc := gob.NewEncoder(writer)
	err = enc.Encode(s.hashes)
	s.m.Unlock()
	return err
}

func (service *myDrive) filesBy(q fileQuery) <-chan []*drive.File {
	ch := make(chan []*drive.File)
	var fileMtx sync.Mutex
	service.wg.Add(1)
	go func() {
		defer service.wg.Done()
		done := false
		nextToken := ""
		for {
			service.m.Lock()
			<-service.throttler

			fileMtx.Lock()
			if done {
				service.m.Unlock()
				break
			}
			service.wg.Add(1)
			go func() {
				defer service.wg.Done()
				res, err := service.inner.Files.List().
					Q(q.text).
					PageSize(q.pageSize).
					PageToken(nextToken).
					Fields(q.fields).
					Do()
				if err != nil {
					log.Printf("Unable to retrieve files for query %s. Error: %v\n", q.text, err)
					done = true
					fileMtx.Unlock()
					return
				}
				if len(res.Files) == 0 {
					ch <- []*drive.File{}
				} else {
					var filtered []*drive.File
					for _, f := range res.Files {
						if q.filter(f) {
							filtered = append(filtered, f)
						}
					}
					ch <- filtered
				}
				nextToken = res.NextPageToken
				if nextToken == "" {
					done = true
				}
				fileMtx.Unlock()
			}()

			service.m.Unlock()
		}
		close(ch)
	}()
	return ch
}

func (service *myDrive) downloadTree(q fileQuery, folderPath string) {
	if err := createDir(folderPath); err != nil {
		log.Printf("Impossible to create %s folder. All files in this directory won't be downloaded. Error: %v\n", folderPath, err)
		return
	}

	for files := range service.filesBy(q) {
		for _, f := range files {
			service.wg.Add(1)
			go func(f *drive.File, folderPath string, querymetadata fileQuery) {
				defer service.wg.Done()
				service.dispatch(f, folderPath, querymetadata)
			}(f, folderPath, q)
		}
	}
}

func (service *myDrive) dispatch(f *drive.File, folderPath string, querymetadata fileQuery) {
	mime, isGoogleMime := googleToCanonicalMime[f.MimeType]
	if f.MimeType == "application/vnd.google-apps.folder" {
		service.wg.Add(1)
		newName := createUniquePath(folderPath, f.Name, f.ModifiedTime)
		go func(folderId string, folderPath string) {
			defer service.wg.Done()
			// changing only the text property to preserve the original query configuration
			querymetadata.text = fmt.Sprintf("'%s' in parents and trashed = false", folderId)
			service.downloadTree(querymetadata, folderPath)
		}(f.Id, filepath.Join(folderPath, newName))
		return
	} else if f.MimeType == "application/vnd.google-apps.form" {
		log.Printf("Unable to export file %s because Google forms are not exportable\n", f.Name)
		return
	}

	var d downloader = service.inner.Files.Export(f.Id, mime)
	if ! isGoogleMime {
		d = service.inner.Files.Get(f.Id)
	}
	if service.s.isNewFile(folderPath, f) {
		service.m.Lock()
		<-service.throttler
		service.wg.Add(1)
		go func(d downloader, folderPath string, f *drive.File) {
			defer service.wg.Done()
			service.download(d, folderPath, f)
		}(d, folderPath, f)
		service.m.Unlock()
	}
}

func (service *myDrive) download(d downloader, folderPath string, f *drive.File) {
	log.Printf("--> Downloading file Name: %s Mime: %s to path %s <--\n", f.Name, f.MimeType, folderPath)
	res, err := d.Download()
	if err != nil {
		log.Printf("Unable to export/download file: %s. Error: %v\n", f.Name, err)
		return
	}
	defer res.Body.Close()

	fPath := createUniquePath(folderPath, f.Name+mimeToExt[f.MimeType], f.ModifiedTime)
	destPath := filepath.Join(folderPath, fPath)
	fd, err := os.Create(destPath)
	if err != nil {
		log.Printf("Unable to create file: %s. Error: %v\n", destPath, err)
		return
	}
	defer fd.Close()

	if _, err = io.Copy(fd, res.Body); err != nil {
		log.Printf("Unable to write to file %s. Error: %v\n", destPath, err)
		return
	}
	if err = service.s.save(destPath, f); err != nil {
		log.Printf("Unable to persist state for file %s. Error: %v\n", destPath, err)
	}
}

func main() {
	log.Println("Process started")

	throttling := flag.Int64("throttling", 250, "Interval between google drive API requests")
	flag.Parse()
	log.Printf("Setting throttling to %d\n", *throttling)

	if err := createDir(DefaultDstdFolder); err != nil {
		log.Fatalf("Impossible to create %s folder. Error: %v\n", DefaultDstdFolder, err)
	}

	var fields googleapi.Field = "nextPageToken, files(id, name, parents, mimeType, modifiedTime)"
	var pageSize int64 = 100
	var always = func(file *drive.File) bool {
		return true
	}
	myDriveQuery := fileQuery{
		fmt.Sprintf("'root' in parents and trashed = false"),
		fields,
		pageSize,
		always,
	}
	sharedWithMeQuery := fileQuery{
		"sharedWithMe and trashed = false",
		fields,
		pageSize,
		always,
	}
	orphanedQuery := fileQuery{
		fmt.Sprintf("'me' in owners and trashed = false"),
		fields,
		pageSize,
		func(file *drive.File) bool {
			return len(file.Parents) == 0
		},
	}

	service := &myDrive{inner: google.NewDrive(), throttler: time.Tick(time.Duration(*throttling) * time.Millisecond), s: initState()}
	service.downloadTree(myDriveQuery, filepath.Join(DefaultDstdFolder, "myDrive"))
	service.downloadTree(sharedWithMeQuery, filepath.Join(DefaultDstdFolder, "sharedWithMe"))
	service.downloadTree(orphanedQuery, filepath.Join(DefaultDstdFolder, "orphaned"))
	service.wg.Wait()

	log.Println("All goroutines ended.")

	// Renaming file with duplicates
	for oldName, newName := range fileTorename {
		err := os.Rename(oldName, newName)
		if err != nil {
			log.Printf("Impossible rename file %s to %s. Error: %v\n", oldName, newName, err)
		}
	}

	log.Println("Process has finished.")

}
